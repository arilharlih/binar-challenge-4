import React from "react";
import { Route, Routes } from "react-router-dom";
import { SearchCarPage, CarDetailPage } from "../pages/CarPage/CarPage";
import { Homepage } from "../pages/Homepage/Homepage";
import { Test } from "../pages/TestPage/Test";

export const Routers = () => {
  return (
    <Routes>
      <Route path="/" element={<Homepage />} />
      <Route path="/search" element={<SearchCarPage />} />
      <Route path="/car" element={<CarDetailPage />}>
        <Route path="/car/:id" element={<CarDetailPage />} />
      </Route>
      <Route path="/test" element={<Test />} />
    </Routes>
  );
};
