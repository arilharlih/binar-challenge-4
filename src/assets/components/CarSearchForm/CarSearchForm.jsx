import React, { useEffect, useRef } from "react";
import { Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "./CarSearchForm.scss";

export const CarSearchForm = ({
  tipe,
  tanggal,
  waktu,
  penumpang,
  disable,
  mode,
}) => {
  const navigate = useNavigate();

  const tipeInput = useRef(null);
  const tanggalPesanInput = useRef(null);
  const waktuAmbilInput = useRef(null);
  const jumlahPenumpangInput = useRef(null);

  useEffect(() => {
    if (tipe != null) tipeInput.current.value = tipe;
    if (tanggal != null) tanggalPesanInput.current.value = tanggal;
    if (waktu != null) waktuAmbilInput.current.value = waktu;
    if (penumpang != null) jumlahPenumpangInput.current.value = penumpang;
  }, []);

  const handleSubmitSearch = (e) => {
    e.preventDefault();

    navigate(
      `/search?tipe=${tipeInput.current.value}&tanggal=${tanggalPesanInput.current.value}&waktu=${waktuAmbilInput.current.value}&penumpang=${jumlahPenumpangInput.current.value}`
    );
  };

  return (
    <div className="car-search">
      <Form className="form-car-search" onSubmit={handleSubmitSearch}>
        <Form.Group className="form-group">
          <Form.Label>Tipe Driver</Form.Label>
          <Form.Select
            className="input-field"
            id="formCarType"
            name="typeCar"
            ref={tipeInput}
            disabled={disable}
          >
            <option value="" hidden>
              Pilih Tipe Driver
            </option>
            <option value="0">Tanpa Driver</option>
            <option value="1">Dengan Driver</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className="form-group">
          <Form.Label>Tanggal</Form.Label>
          <Form.Control
            ref={tanggalPesanInput}
            type="date"
            placeholder="Pilih Tanggal"
            className="input-field"
            id="formTanggal"
            name="tanggal"
            disabled={disable}
          />
        </Form.Group>
        <Form.Group className="form-group">
          <Form.Label>Waktu Jemput/Ambil</Form.Label>
          <Form.Control
            ref={waktuAmbilInput}
            type="time"
            placeholder="Pilih waktu jemput"
            className="input-field"
            id="formWaktuAmbil"
            name="waktuAmbil"
            disabled={disable}
          />
        </Form.Group>
        <Form.Group className="form-group">
          <Form.Label>Jumlah Penumpang (optional)</Form.Label>
          <Form.Control
            ref={jumlahPenumpangInput}
            type="number"
            min={1}
            placeholder="Jumlah Penumpang"
            className="input-field"
            id="formJumlahPenumpang"
            name="jumlahPenumpang"
            disabled={disable}
          />
        </Form.Group>

        {!disable ? (
          <Button
            type="submit"
            variant={mode === "edit" ? "outline-primary" : "success"}
          >
            {mode === "edit" ? "Edit" : "Cari Mobil"}
          </Button>
        ) : (
          ""
        )}
      </Form>
    </div>
  );
};
