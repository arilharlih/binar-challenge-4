import React, { useEffect, useState } from "react";
import { Accordion, Button } from "react-bootstrap";
import { useLocation, useParams } from "react-router-dom";
import { CardCarItem } from "../../assets/components/CardCarItem/CardCarItem";
import { CarSearchForm } from "../../assets/components/CarSearchForm/CarSearchForm";
import { ReactComponent as CalendarIcon } from "../../assets/img/fi_calendar.svg";
import { ReactComponent as SettingIcon } from "../../assets/img/fi_settings.svg";
import { ReactComponent as UsersIcon } from "../../assets/img/fi_users.svg";
import "./CarPage.scss";

export const SearchCarPage = () => {
  let query = useQuery();

  const [carData, setCarData] = useState([]);

  useEffect(() => {
    const axios = require("axios");
    axios.get("https://rent-cars-api.herokuapp.com/admin/car").then((res) => {
      setCarData(res.data);
    });
  }, []);

  const filterCar = () => {
    const penumpang = query.get("penumpang");
    const tipe = query.get("tipe");
    const tanggal = query.get("tanggal");
    const waktu = query.get("waktu");

    // filter driver
    let carByTipe = [];
    if (tipe === "") {
      carByTipe = carData;
    } else {
      carByTipe = carData.filter((car) => {
        if (tipe === "0") {
          return !car.status;
        } else if (tipe === "1") {
          return car.status;
        }
      });
    }

    // filter by penumpang
    let carByPenumpang = [];
    if (penumpang === "") {
      carByPenumpang = carByTipe;
    } else {
      carByPenumpang = carByTipe.filter((car) => {
        if (penumpang === "1") {
          return car.category.toLowerCase() === "small";
        } else if (penumpang > 1 && penumpang < 4) {
          return car.category.toLowerCase() === "medium";
        } else {
          return car.category.toLowerCase() === "large";
        }
      });
    }

    return carByPenumpang;
  };

  return (
    <div className="car-page">
      <div className="banner"></div>
      <div className="content-container d-flex flex-column align-items-center">
        <CarSearchForm
          tipe={query.get("tipe")}
          tanggal={query.get("tanggalPesan")}
          waktu={query.get("waktuAmbil")}
          penumpang={query.get("penumpang")}
          mode="edit"
        />
        <div className="car-list">
          {filterCar().map((car) => {
            return (
              <CardCarItem
                key={car.name + car.id}
                id={car.id}
                img={car.image}
                name={car.name}
                type={car.category}
                desc={
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                }
                seat={4}
                gear={"Manual"}
                price={car.price}
                year={2022}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export const CarDetailPage = () => {
  const params = useParams();

  const [carData, setCarData] = useState({});

  useEffect(() => {
    const axios = require("axios");
    axios
      .get(`https://rent-cars-api.herokuapp.com/admin/car/${params.id}`)
      .then((res) => {
        setCarData(res.data);
      });
  }, []);

  const convertPriceToRupiah = (price) => {
    return Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    })
      .format(price)
      .split(",")[0];
  };

  return (
    <div className="car-page">
      <div className="banner"></div>
      <div className="content-container d-flex flex-column align-items-center">
        <CarSearchForm disable />
        <div className={`main-content ${carData ? "" : "d-none"}`}>
          <div className="section-1">
            <div className="description-section">
              <Accordion defaultActiveKey={["0", "1"]} alwaysOpen>
                <Accordion.Item eventKey="0">
                  <Accordion.Header className="header">
                    Tentang Paket
                  </Accordion.Header>
                  <Accordion.Body>
                    <div className="list-group">
                      <h4>Include</h4>
                      <ul>
                        <li>
                          Apa saja yang termasuk dalam paket misal durasi max 12
                          jam
                        </li>
                        <li>Sudah termasuk bensin selama 12 jam</li>
                        <li>Sudah termasuk Tiket Wisata</li>
                        <li>Sudah termasuk pajak</li>
                      </ul>
                    </div>
                    <div className="list-group">
                      <h4>Exclude</h4>
                      <ul>
                        <li>
                          Apa saja yang termasuk dalam paket misal durasi max 12
                          jam
                        </li>
                        <li>Sudah termasuk bensin selama 12 jam</li>
                        <li>Sudah termasuk Tiket Wisata</li>
                        <li>Sudah termasuk pajak</li>
                      </ul>
                    </div>
                  </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                  <Accordion.Header>
                    Refund, Reschedule, Overtime
                  </Accordion.Header>
                  <Accordion.Body>
                    <div className="list-group">
                      <ul>
                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                        <li>
                          Jika overtime lebih dari 12 jam akan ada tambahan
                          biaya Rp 20.000/jam
                        </li>
                        <li>Tidak termasuk akomodasi penginapan</li>
                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                        <li>
                          Jika overtime lebih dari 12 jam akan ada tambahan
                          biaya Rp 20.000/jam
                        </li>
                        <li>Tidak termasuk akomodasi penginapan</li>
                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                        <li>
                          Jika overtime lebih dari 12 jam akan ada tambahan
                          biaya Rp 20.000/jam
                        </li>
                        <li>Tidak termasuk akomodasi penginapan</li>
                      </ul>
                    </div>
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
            </div>
            <Button variant="success">Lanjutkan Pembayaran</Button>
          </div>

          <div className="specs-section">
            <div className="car-image">
              <img src={carData?.image} alt="Car" />
            </div>
            <div className="description">
              <div className="car-description">
                <h3>
                  {carData?.name}/{carData?.category}
                </h3>
                <div className="specs-desc">
                  <span className="specs-item">
                    <UsersIcon />4 orang
                  </span>
                  <span className="specs-item">
                    <SettingIcon />
                    Manual
                  </span>
                  <span className="specs-item">
                    <CalendarIcon />
                    Tahun 2020
                  </span>
                </div>
              </div>
              <hr />
              <div className="price-section">
                <span>Total</span>
                <h3>{convertPriceToRupiah(carData?.price)}</h3>
              </div>
              <Button variant="success">Lanjutkan Pembayaran</Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

/* Global Function */
function useQuery() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}
